﻿using System;

namespace MethodsTask2
{
    public class Net
    {
        public Net(double maxX, double maxY, double maxT, double h, double t)
        {
            MaxX = maxX;
            MaxY = maxY;
            MaxT = maxT;
            H = h;
            T = t/2;
            SizeX = (int)(maxX / h);
            SizeY = (int)(maxY / h);
            Height = (int)(maxT / T);
            _net = new double[SizeX + 1, SizeY + 1, Height + 1];


            for (var j = 0; j <= SizeY; j++)
            {
                for (var i = 0; i <= SizeX; i++)
                {
                    var initialValue = Initial(i * H, j * H);
                    Set(i, j, 0, initialValue);
                }
            }

            for (var k = 0; k <= Height; k++)
            {
                for (var j = 0; j <= SizeY; j++)
                {
                    var value = BorderY(j * H, k * T);
                    Set(0, j, k, value);
                    Set(SizeX, j, k, value);
                }
                for (var i = 0; i <= SizeX; i++)
                {
                    var value = BorderX(i * H, k * T);
                    Set(i, 0, k, value);
                    Set(i, SizeY, k, value);
                }
            }
        }

        public double MaxX { get; }
        public double MaxY { get; }
        public double MaxT { get; }
        public double H { get; }
        public double T { get; }
        public int SizeX { get; }
        public int SizeY { get; }
        public int Height { get; }

        private readonly double[,,] _net;

        public void Set(int i, int j, int k, double value)
        {
            _net[i, j, k] = value;
        }
        public double Get(int i, int j, int k)
        {
            return _net[i, j, k];
        }

        private double Initial(double x, double y)
        {
            return Math.Sin(x + y);
        }

        private static double BorderY(double y, double t)
        {
            return Math.Sin(y) + Math.Log(t * t + 1);
        }

        private static double BorderX(double x, double t)
        {
            return Math.Sin(x) + Math.Log(t * t + 1);
        }
    }
}
