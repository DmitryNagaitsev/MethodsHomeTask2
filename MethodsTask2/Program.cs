﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace MethodsTask2
{
    public class Program
    {
        static void Main()
        {
            for (var i = 0; i < 5; i++)
            {
                var net = new Net(2 * Math.PI, 2 * Math.PI, 10, Math.PI / (5 * Math.Pow(2, i)), 0.25);
                Compute(net);
                var error = GetError(net, (x, y, t) => Math.Sin(x + y) + Math.Log(t * t + 1));
                Console.WriteLine("(PI/{0}, PI/{0}, {1})={2:g4}", 5 * Math.Pow(2, i), 0.25, error);
            }

            Console.WriteLine();

            for (var j = 0; j < 5; j++)
            {
                var net = new Net(2 * Math.PI, 2 * Math.PI, 10, Math.PI / 5, 0.25 / Math.Pow(2, j));
                Compute(net);
                var error = GetError(net, (x, y, t) => Math.Sin(x + y) + Math.Log(t * t + 1));
                Console.WriteLine("(PI/{0}, PI/{0}, {1})={2:g4}", 5, 0.25 / Math.Pow(2, j), error);
            }

            Console.WriteLine();

            for (var j = 1; j < 4; j++)
            {
                var net = new Net(2 * Math.PI, 2 * Math.PI, 10, Math.PI / (5 * Math.Pow(2, j)), 0.25 / Math.Pow(2, j));
                Compute(net);
                var error = GetError(net, (x, y, t) => Math.Sin(x + y) + Math.Log(t * t + 1));
                Console.WriteLine("(PI/{0}, PI/{0}, {1})={2:g4}", 5 * Math.Pow(2, j), 0.25 / Math.Pow(2, j), error);
            }
        }

        public static double F(double x, double y, double t)
        {
            return 2 * Math.Sin(x + y) + 2 * t / (t * t + 1);
        }

        public static void Compute(Net net)
        {
            for (var k = 1; k <= net.Height; k++)
            {
                //прогонку по оси у
                for (var i = 1; i < net.SizeX; i++)
                {
                    var column = RunY(net, i, k);
                    for (var j = 1; j < net.SizeY; j++)
                    {
                        net.Set(i, j, k, column[j]);
                    }
                }
                //прогонка по оси х
                for (var j = 1; j < net.SizeY; j++)
                {
                    var row = RunX(net, j, k);
                    for (var i = 1; i < net.SizeX; i++)
                    {
                        net.Set(i, j, k, row[i]);
                    }
                }
            }
        }

        private static double[] RunY(Net net, int i, int k)
        {
            var ai = -(net.T / (net.H * net.H)) / 2;
            var bi = 1 + net.T / (net.H * net.H);
            var ci = ai;

            var n = net.SizeY - 1;
            var matrix = new double[n, n];
            var values = new double[n];

            for (var j = 0; j < n; j++)
            {
                if (j > 0)
                {
                    matrix[j - 1, j] = ai;
                }
                matrix[j, j] = bi;
                if (j < n - 1)
                {
                    matrix[j + 1, j] = ci;
                }

                values[j] = Fj(j + 1);
            }

            values[0] -= net.Get(i, 0, k) * ai;
            values[n - 1] -= net.Get(i, net.SizeY, k) * ci;
            var tdma = TridiagonalMatrixAlgoritm(matrix, values);
            var result = new double[net.SizeY + 1];
            for (var j = 0; j < tdma.Length; j++) result[j + 1] = tdma[j];
            return result;

            double Fj(int j)
            {
                var aj = net.T / (net.H * net.H) / 2;
                var bj = 1 - net.T / (net.H * net.H);
                var cj = aj;
                return aj * net.Get(i, j - 1, k - 1) + bj * net.Get(i, j, k - 1) + cj * net.Get(i, j + 1, k - 1) +
                       net.T / 2 * F(i * net.H, j * net.H, k * net.T);
            }
        }

        private static double[] RunX(Net net, int j, int k)
        {
            var aj = -(net.T / (net.H * net.H)) / 2;
            var bj = 1 + net.T / (net.H * net.H);
            var cj = aj;

            var n = net.SizeX - 1;
            var matrix = new double[n, n];
            var values = new double[n];

            for (var i = 0; i < n; i++)
            {
                if (i > 0)
                {
                    matrix[i - 1, i] = aj;
                }
                matrix[i, i] = bj;
                if (i < n - 1)
                {
                    matrix[i + 1, i] = cj;
                }

                values[i] = Fi(i + 1);
            }

            values[0] -= net.Get(0, j, k) * aj;
            values[n - 1] -= net.Get(net.SizeX, j, k) * cj;

            var tdma = TridiagonalMatrixAlgoritm(matrix, values);
            var result = new double[net.SizeX + 1];
            for (var i = 0; i < tdma.Length; i++) result[i + 1] = tdma[i];
            return result;

            double Fi(int i)
            {
                var ai = net.T / (net.H * net.H) / 2;
                var bi = 1 - net.T / (net.H * net.H);
                var ci = ai;
                return ai * net.Get(i - 1, j, k) + bi * net.Get(i, j, k) + ci * net.Get(i + 1, j, k) +
                       net.T / 2 * F(i * net.H, j * net.H, (k - 1) * net.T);
            }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public static double[] TridiagonalMatrixAlgoritm(double[,] A, double[] B)
        {
            var N = B.Length - 1;
            var n = B.Length;
            var alpha = new double[N + 1];
            var beta = new double[N + 2];

            alpha[1] = -A[1, 0] / A[0, 0];
            beta[1] = B[0] / A[0, 0];

            for (var i = 1; i <= N; i++)
            {
                var a = A[i - 1, i];
                var c = A[i, i];
                var f = B[i];

                if (i < N)
                {
                    var b = A[i + 1, i];
                    alpha[i + 1] = -b / (c + alpha[i] * a);
                }
                beta[i + 1] = (f - a * beta[i]) / (c + a * alpha[i]);
            }

            var x = new double[n];
            x[N] = beta[N + 1];
            for (var i = N - 1; i >= 0; i--)
            {
                x[i] = alpha[i + 1] * x[i + 1] + beta[i + 1];
            }

            return x;
        }


        public static double GetError(Net net, Func<double, double, double, double> expected)
        {
            var result = 0d;
            for (var k = 0; k <= net.Height; k++)
            {
                for (var j = 0; j <= net.SizeY; j++)
                {
                    for (var i = 0; i <= net.SizeX; i++)
                    {
                        var x = net.H * i;
                        var y = net.H * j;
                        var t = net.T * k;
                        var error = Math.Abs(expected(x, y, t) - net.Get(i, j, k));
                        if (error > result)
                        {
                            result = error;
                        }
                    }
                }
            }
            return result;
        }
    }
}
